use std::io;

fn main() {
    // let mut temperatures : [f32; 7] = [22.0, 19.5, 21.0, 23.5, 20.0, 18.0, 25.0];
    // let temperatures: [f32; 0] = [];

    let mut temperatures = [0.0; 7];

    for i in 0..temperatures.len() {
        println!("Entrer une temperature {}", i + 1);
        let mut input = String::new();

        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");

        temperatures[i] = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => panic!("Please type a number!"),
        };
    }

    println!("Temperatures: {:?}", temperatures);

    if temperatures.len() == 0 {
        println!("Aucune température");
        return;
    }

    let average = calculate_average(&temperatures);
    println!("Température moyenne : {:.2}°C", average);

    let cat_team = get_cat_temp(average);

    let temp_moyenne = TemperatureMoyenne {
        categorie: cat_team,
        temperature_moyenne: average,
    };
    break_down(temp_moyenne);

    sort_array(&mut temperatures);
}

fn sort_array(arr: &mut [f32]) {
    println!("Tri des températures par ordre croissant");

    arr.sort_by(|a, b| a.partial_cmp(b).unwrap());

    for temp in arr {
        println!("{temp}");
    }
}

fn break_down(temp_data: TemperatureMoyenne) {
    match temp_data.categorie {
        Category::Froid => print_it(temp_data.temperature_moyenne, "froide".to_string()),
        Category::Tempere => print_it(temp_data.temperature_moyenne, "tempérée".to_string()),
        Category::Chaud => print_it(temp_data.temperature_moyenne, "chaude".to_string()),
    }
}

fn print_it(temp: f32, cat: String) {
    println!("La température à {:.2} est de type -> {}", temp, cat);
}

fn calculate_average(temps: &[f32]) -> f32 {
    let somme: f32 = temps.iter().sum();
    somme / temps.len() as f32
}

enum Category {
    Froid,
    Tempere,
    Chaud,
}

struct TemperatureMoyenne {
    temperature_moyenne: f32,
    categorie: Category,
}

fn get_cat_temp(average_temp: f32) -> Category {
    let cat: Category;
    match average_temp {
        tmp if tmp > 20.0 as f32 => cat = Category::Chaud,
        tmp if tmp < 9.9 as f32 => cat = Category::Froid,
        _ => cat = Category::Tempere,
    }
    cat
}
